cmake_minimum_required(VERSION 3.0)

project(FPE_project)

# set compiler flag
set( CMAKE_BUILD_TYPE Debug )

# the header directory include all the header files
include_directories(include)

# collect all *.cpp source files
file(GLOB SOURCES "src/*.cpp")


add_executable(2DFPE ${SOURCES})

target_compile_features(2DFPE PRIVATE cxx_constexpr)
target_link_libraries(2DFPE armadillo hdf5 hdf5_cpp)

