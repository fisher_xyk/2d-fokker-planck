#include <cctype>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <math.h>

#include "component.hpp"
#include "io.hpp"
#include <armadillo>
#include "constants.hpp"


Field::Field(arma::rowvec orient_new): amplitude(1.0), Orient(orient_new) 
{ 
        Orient = norm(Orient) == 0.0 ? Orient : normalise(Orient); 
}
                
Field::Field(double amp, arma::rowvec orient_new)
        : amplitude(amp), Orient(orient_new) 
{ 
        Orient = norm(Orient) == 0.0 ? Orient : normalise(Orient); 
}

Current::Current(double pol_in, double curr_amp,
                 arma::rowvec CurrOrient,
                 arma::rowvec SpinOrient):
        Pol(pol_in),
        Curr(curr_amp, CurrOrient),
        Spin(SpinOrient)
{}


Magnet::Magnet():
        damping(0.01),
        dims( arma::rowvec({40e-9, 40e-9, 2e-9}) ),
        demag( arma::rowvec({0.0,  0.0,  0.0}) ),
        magnetization( 9e5, arma::rowvec({0.0, 0.0, 1.0}) ),
        anisotropy( 0.0, arma::rowvec({0.0, 0.0, 1.0}) ),
        OutOfPlane(0.0)
{}

/* helper function: comparing strings, case insensitive */
bool string_cmp(const std::string& lhs, const std::string& rhs)
{
    unsigned int ls = lhs.size();
    if (rhs.size() != ls) return false;
    for (unsigned int i=0; i < ls; i++)
        if ( std::tolower(lhs[i]) != std::tolower(rhs[i]) )
            return false;
    return true;
}


void Magnet::read_file(const std::string& filename)
{
         std::ifstream fin(filename);
         std::string line, key;
         double temperature = -1.0;                       // temperarily store temperature parameter
         if (fin.is_open())
         {
                while (std::getline(fin,line))
                {
                        std::istringstream iss(line);
                        iss >> key;
                        if ( is_comment(key) ) continue;
                        int category = read_category(key);
                        if ( category == -1 or category == 1 ) continue;
                        key = strip_category(key);
                        if (string_cmp(key, "temperature") )
                                temperature = read_double(line);
                        else if (string_cmp(key,"Ms"))
                                magnetization.setAmplitude( read_double(line) );
                        else if (string_cmp(key,"MagneticMoment"))  
                                magnetization.setOrient( read_vector(line, 3) );
                        else if (string_cmp(key,"damping"))  
                                damping = read_double(line);
                        else if (string_cmp(key,"OutOfPlane"))  
                                OutOfPlane = read_double(line);
                        else if (string_cmp(key,"dimensions")) 
                                dims = read_vector(line, 3);
                        else if (string_cmp(key,"DemagCoefficient")) 
                                demag.setOrient( read_vector(line, 3) );
                        else if (string_cmp(key,"anisotropy"))
                                anisotropy.setAmplitude( read_double(line) );
                        else if (string_cmp(key,"EasyAxis"))  
                                anisotropy.setOrient( read_vector(line, 3) );
                        else if (string_cmp(key,"thermalstability")) {
                                if ( temperature < 0 ) {
                                std::cout << "Warning: temperature parameter not found!"
                                             << " Temperature set to default value 300K." << std::endl;
                                        temperature = 300;
                                }
                                double stability = read_double(line);
                                anisotropy.setAmplitude( C_kB * temperature * stability / getVolume() );                        
                        }
                        else {
                           std::cout << "Invalid parameter: (" 
                                     << key << ") for category [mag]!" << std::endl;
                        }
                }
                fin.close();
         }
         else std::cout << "Unable to open input file: " 
                        << filename << std::endl;
}

void Magnet::print_info(double temperature) const
{
        std::cout << "- magnet parameters" << std::endl;
        std::cout << "  Ms: " << magnetization.getAmplitude() << std::endl;
        std::cout << "  alpha: " << damping << std::endl;
        std::cout << "  Dimensions: " << "[" << dims << "]" << std::endl;
        std::cout << "  Hk: " << 2.0*anisotropy.getAmplitude() / (C_u0 * magnetization.getAmplitude())  << std::endl;
        std::cout << "  Eb/kT: " << "[" << delta_xyz(temperature) << "]" << std::endl;
        std::cout << "  Initial Mag: " << "[" << magnetization.getOrient() << "]" << std::endl;
        std::cout << "  Uniaxial Ku: " << "[" <<  anisotropy.getOrient() * anisotropy.getAmplitude() << "]" << std::endl;
        std::cout << "  Demagnetization: " << "[" << demag.getOrient() << "]" << std::endl;
}


arma::rowvec Magnet::delta_xyz(double temperature) const {
        arma::rowvec Exyz(3);
        double Ms = magnetization.getAmplitude();
        Exyz(0) = 0.5 * C_u0 * Ms * Ms * getVolume() * demag.getOrient()[0] / ( C_kB*temperature );
        Exyz(1) = 0.5 * C_u0 * Ms * Ms * getVolume() * demag.getOrient()[1] / ( C_kB*temperature );
        Exyz(2) = anisotropy.getAmplitude() * anisotropy.getOrient()(2) * getVolume() / (C_kB*temperature);
        return Exyz;
}

arma::rowvec Magnet::getLocalTorq(const arma::rowvec& point, const Current &curr) const
{
        arma::rowvec::fixed<3> L_inplane;
        double s1 = curr.getCurrAmp(), 
               s2 = OutOfPlane * s1, Vol = getVolume(),
               Ms = magnetization.getAmplitude();
        arma::rowvec torq = curr.getSpinOrient();
        arma::rowvec P1 = arma::normalise(point);
        arma::rowvec Heff = -1.0 * Ms * ( getEffectiveDemag() % P1 );
        //Heff.print("Heff:");
        L_inplane = -1.0/(1.0+damping*damping) * (
                        + damping*C_gyro * arma::cross(P1,arma::cross(P1,Heff)) // damping
                        + C_gyro * arma::cross(P1,Heff) // prcession term
                        + ( C_muB*s1 + damping*C_muB*s2 ) / (Vol*Ms*C_qe) 
                                * arma::cross(P1, arma::cross(P1, torq)) // spin torque
                        + ( C_muB*s2 - damping*C_muB*s1 ) / (Vol*Ms*C_qe)
                                * arma::cross(P1, torq)
                    ); // spin torque2
        //L_inplane.print("L_inplane:");
        return L_inplane;
} 


System::System():
        mag(),
        injectCurr(),
        hext(),
        temperature(300.0),
        time(0.1e-9),
        dt(1e-11),
        nData(20)
{}
        
void System::read_file(const std::string& filename)
{
        std::cout << "Read magnetic parameters..." << std::endl;
        mag.read_file(filename);

        std::ifstream fin(filename);
        std::string line, key;
        if (fin.is_open())
        {
               while (std::getline(fin,line))
               {
                       std::istringstream iss(line);
                       iss >> key;
                       if ( is_comment(key) ) continue;
                       int category = read_category(key);
                       if ( category == -1 or category == 0 ) continue;
                       key = strip_category(key);

                       if ( string_cmp(key, "temperature") )
                               temperature = read_double(line);
                       else if (string_cmp(key,"Hext")) 
                               hext.setAmplitude( read_double(line) );
                       else if (string_cmp(key,"HextDirection"))  
                               hext.setOrient( read_vector(line, 3) );
                       else if (string_cmp(key,"current"))
                               injectCurr.setCurrAmp( read_double(line) );
                       else if (string_cmp(key,"currentPol"))
                               injectCurr.setPol( read_double(line) );
                       else if (string_cmp(key,"NcritCurr")) {
                               injectCurr.setCurrAmp( read_double(line) *
                                                      mag.getCritCurr() / 
                                                      injectCurr.getPol() );
                       }
                       else if (string_cmp(key,"currentDirection")) 
                               injectCurr.setCurrOrient( read_vector(line, 3) );
                       else if (string_cmp(key,"currentSpin")) 
                               injectCurr.setSpinOrient( read_vector(line, 3) );
                       else if (string_cmp(key,"Time"))
                               time = read_double(line);
                       else if (string_cmp(key,"Time_step"))
                               dt = read_double(line);
                       else if (string_cmp(key,"Num_data_points"))
                               nData = read_int(line);
                       else {
                           std::cout << "Invalid parameter: (" 
                                     << key << ") for category [sys]!" << std::endl;
                       }
               }
               fin.close();
        }
        else std::cout << "Unable to open input file: " 
                       << filename << std::endl;

}


void System::print_info() const
{
    std::cout << "****** System Parameters ******" << std::endl;
    mag.print_info(temperature);
    std::cout << "- Other parameters" << std::endl;
    std::cout << "  External H: " << "[" << hext.getAmplitude() * hext.getOrient() << "]" << std::endl;
    std::cout << "  Current : " << injectCurr.getCurrAmp() << std::endl;
    std::cout << "  Spin direction : " << "[" << injectCurr.getSpinOrient() << "]" << std::endl;
    std::cout << "****** End of the parameters ******" << std::endl;
}

std::vector<double> System::sim_time() {
    std::vector<double> time_grid;
    if (time <= dt){
        std::cout << "Error: total time smaller than the time step!" << std::endl;
        return time_grid;
    }
    double tick = 0;
    while(tick <= time){
        time_grid.push_back(tick);
        tick += dt;
    }
    return time_grid;
}

std::vector<unsigned int> System::record_inds(const std::vector<double> &time_grid) {
    std::vector<unsigned int> rec_ind;
    if ( nData > time_grid.size() ){
        std::cout << "Error: record size larger than time grid!" << std::endl;
        return rec_ind;
    }
    unsigned int gap = time_grid.size() / nData;
    for (unsigned int ind=0; ind < time_grid.size()-1; ind++){
        if ( ind % gap == 0 ) rec_ind.push_back(ind);
    }
    rec_ind.push_back( time_grid.size()-1 );
    return rec_ind;
}
