#include <armadillo>

#include "Post.hpp"
#include "galerkin.hpp"

std::vector<double> NSwitch_prob( arma::mat& Data, FEM_space& FEM, Magnet& mag){
        unsigned int Nt = FEM.Grid_Nmeshes();
        arma::Mat<unsigned int>* tri = FEM.triang();
        arma::mat pp = *(FEM.vertices());
        arma::rowvec  M0 = mag.Minit();
        unsigned int Ndat = Data.n_cols;

        arma::Row<unsigned int>::fixed<3> t_k;
        arma::rowvec::fixed<3> Pc;
        std::vector<double> NSW( Ndat );

        double sum=0.0;
        for (unsigned int nd=0; nd<Ndat; nd++){
            sum = 0.0;
            for (unsigned int tt=0; tt<Nt; tt++){
                t_k = tri->row(tt) - 1;
                Pc = ( pp.row(t_k(0)) + pp.row(t_k(1)) + pp.row(t_k(2)) )/3.0;
                if ( arma::dot( Pc,M0 ) > 0 )
                    sum += integrate_node( pp, t_k, Data.col(nd) );
            }
            NSW[nd] = sum;
        } 
        return NSW;
}
