#include "io.hpp"

        
int read_int(std::string line)
{
        std::string ThrowAwayKey;
        int val;
        std::stringstream ss(line);
        ss >> ThrowAwayKey >> val;
        return val;
}

double read_double(std::string line)
{
        std::string ThrowAwayKey;
        double val;
        std::stringstream ss(line);
        ss >> ThrowAwayKey >> val;
        return val;
}

std::string read_string(std::string line)
{
        std::string ThrowAwayKey, val;
        std::stringstream ss(line);
        ss >> ThrowAwayKey >> val;
        return val;
}

arma::rowvec read_vector(std::string line, int length)
{
        arma::rowvec dvec(length, arma::fill::zeros);
        std::string val, ThrowAwayKey;
        std::stringstream ss(line);
        ss >> ThrowAwayKey;
        for (int pos=0; pos<length or !ss.eof(); pos++) 
        {
                getline(ss, val, ',');
                dvec(pos) = stod(val);
        }
        return dvec;
}

int read_category(const std::string key) // 0 for mag, 1 for sys, -1 for the rest
{
        std::string category;
        bool record = false;
        for ( const auto& cc : key )
        {
                if ( cc == '[' ) {
                        record = true;
                        continue;
                }
                if ( cc == ']' ) break;
                if ( record ) category.append(&cc,1);
        }
        if ( category.compare(std::string("mag")) == 0 ) return 0;
        else if ( category.compare(std::string("sys")) == 0 ) return 1;
        else return -1;
}

bool is_comment(const std::string key)
{
        if ( key.empty() ) return true;
        if ( key[0] == '#' or key[0] == '%' ) return true;
        return false;
}

std::string strip_category(const std::string key)
{
        int pos = 0;
        while ( pos < key.length() and key[pos] != ']' ) pos++;
        return key.substr(pos+1);
}
