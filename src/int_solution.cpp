#include <math.h>

#include <armadillo>
#include "FEM.hpp"
#include "galerkin.hpp"


double func_phi2( unsigned int p, double u, double v){
    if ( p == 0 ) return ( 1.0 - u - v );
    else if ( p == 1 ) return v;
    else if ( p == 2 ) return u;
    else{
        std::cout << " invalid point for func_phi2! " << std::endl;
        return 0.0;
    }
}

double integrate_node( const arma::mat& p, 
                   const arma::Row<unsigned int>& t_k, 
                   const arma::vec& W ){

        double sum = 0.0;
        arma::rowvec::fixed<3> Xu, Xv;
        // Evaluate the functions from seven integration points
        double u, v, A, B, C, Phi1, Phi2, Phi3; 
        // locations and weights of the integration point
        std::vector<double> k1 = { 1.0/3.0, 0.0597158717, 0.4701420641, 0.4701420641, 0.7974269853, 0.1012865073, 0.1012865073 };
        std::vector<double> k2 = { 1.0/3.0, 0.4701420641, 0.0597158717, 0.4701420641, 0.1012865073, 0.7974269853, 0.1012865073 };
        std::vector<double> k3 = { 1.0/3.0, 0.4701420641, 0.4701420641, 0.0597158717, 0.1012865073, 0.1012865073, 0.7974269853 };
        std::vector<double> weight = { 0.225, 0.1323941527, 0.1323941527, 0.1323941527, 0.1259391805, 0.1259391805, 0.1259391805 };

        Xu = p.row(t_k(2)) - p.row(t_k(0));
        Xv = p.row(t_k(1)) - p.row(t_k(0));

        A = arma::dot( Xu,Xu );
        B = arma::dot( Xu,Xv );
        C = arma::dot( Xv,Xv );

        for (int pp = 0; pp < 7; pp++ ) {
                Phi1 = func_phi2( 0, k3[pp], k2[pp] );
                Phi2 = func_phi2( 1, k3[pp], k2[pp] );
                Phi3 = func_phi2( 2, k3[pp], k2[pp] );
                sum = sum + sqrt( A*C-B*B ) * weight[pp] 
                        * ( Phi1 * W(t_k(0)) + Phi2 * W(t_k(1)) + Phi3 * W(t_k(2)) );
                
        }
                                              
     return sum;
}

