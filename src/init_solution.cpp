#include <math.h>

#include "armadillo"
#include "FEM.hpp"
#include "galerkin.hpp"

arma::vec  InitNodeValue( FEM_space& FEM, System& sys ){
    unsigned int Np = FEM.Grid_Npoints();
    unsigned int Nt = FEM.Grid_Nmeshes();
    arma::vec  W(Np);
    arma::rowvec  M0 = sys.mag.Minit();
    arma::rowvec delta = sys.mag.delta_xyz( sys.getTemperature() );
    arma::Row<unsigned int>::fixed<3> t_k;
    arma::mat pp = *(FEM.vertices());
    arma::Mat<unsigned int>* tt = FEM.triang();

    for ( unsigned i = 0; i < Np; i++ ){
        if ( pp(i,2) < 0 )
            W(i) = exp( -1*( delta(2) + delta(0) * pp(i,0) * pp(i,0)
                             + delta(1) * pp(i,1) * pp(i,1) ));
        else
            W(i) = exp( -1*( delta(2)*(1-pp(i,2)*pp(i,2)) 
                             + delta(0) * pp(i,0) * pp(i,0)
                             + delta(1) * pp(i,1) * pp(i,1) ));
    }
    double sum = 0.0;
    for ( unsigned int k=0; k < Nt; k++ ){
        t_k = tt->row(k) - 1;
        sum += integrate_node( pp, t_k, W );
    }
    W = W / sum;
    return W;
}
