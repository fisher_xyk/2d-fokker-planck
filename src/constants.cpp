#include "constants.hpp"

const double C_PI = 3.14159265358979323846;
const double C_hbar = 1.054571628E-34;
const double C_u0 = C_PI * 4E-7;
const double C_qe = 1.60217662E-19;
const double C_gyro = 2.21E5;
const double C_muB = 9.274E-24;
const double C_kB = 1.3806504E-23;
