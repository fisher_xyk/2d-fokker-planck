#include <iostream>
#include <string>

#include <armadillo>
#include "Post.hpp"
#include "component.hpp"
#include "FEM.hpp"
#include "galerkin.hpp"


int main()
{
        arma::rowvec* torque_test;
        arma::mat* p;
        arma::Mat<unsigned int>* t;

        const std::string FEM_input="FEM_p_t.h5";
        const std::string fout ="FEM_Dat.h5";
        FEM_space FEM;
        FEM.Read_HDF5(FEM_input, "p", "t");
        std::cout << "grid_dim: " << FEM.Grid_Npoints()
                  << "," << FEM.Grid_Nmeshes() << std::endl;

        System  sys;
        sys.read_file("param.dat");
        sys.print_info();

        FEM.galerkin( sys );

        std::vector<double> time_grid = sys.sim_time();
        double dt = sys.get_time_step();
        std::cout << "Total delay: " << time_grid[0] << ", dt: "
                  << time_grid[1] << std::endl;
        std::vector<unsigned int> rec_t = sys.record_inds(time_grid);

        for (int ind=0; ind<rec_t.size(); ind++)
            std::cout << rec_t[ind] << "," ;
        std::cout << std::endl;
        arma::vec W = InitNodeValue( FEM, sys );
        std::cout << "W cols: " << W.n_rows << std::endl;
        arma::mat Data(W.n_rows, rec_t.size());
        std::cout << "Data mat created" << std::endl;
        Data.col(0) = W;
        unsigned int rind = 1;
        std::cout << "Initialized" << std::endl;

        for (int tt=1; tt<time_grid.size(); tt++){
                std::cout << "step: " << tt << std::endl;
                arma::sp_mat mat_left = (*FEM.matB) - 0.5 * dt * (*FEM.matA);
                arma::vec vec_right = ((*FEM.matB) + 0.5 * dt * (*FEM.matA)) * W;
                W = spsolve( mat_left, vec_right, "lapack" );
                if ( tt == rec_t[rind] ){
                    Data.col(rind) = W;
                    rind++;
                }
        }
        std::vector<double> WER = NSwitch_prob( Data, FEM, sys.mag );
        std::cout << "time dimen: " << time_grid.size() 
                  << " WER: " << WER.size() << std::endl;
        for (int test=0; test<WER.size(); test++){
                std::cout << WER[test] << "\t";
        }
        FEM.Write_HDF5(fout, time_grid, WER, Data);
        std::cout << "Program terminates." << std::endl;
        return 0;
}
