#include <iostream>
#include <string>


#include <armadillo>
#include "H5Cpp.h"
#include "FEM.hpp"

using namespace H5;


arma::mat* FEM_space::vertices()
{
    if ( m_p == NULL )
        std::cout << "Error : Empty vertices!" << std::endl;
    return m_p;
}

arma::Mat<unsigned int>* FEM_space::triang()
{
    if ( m_t == NULL )
        std::cout << "Error : Empty meshes!" << std::endl;
    return m_t;
}

int FEM_space::Read_HDF5(const std::string& filename,
                        const std::string& dataset_P,
                        const std::string& dataset_T)
{
   double *pbuff;
   unsigned int  *tbuff;
   try
   {
        H5std_string FILE_NAME( filename );
        H5std_string PDATA_NAME( dataset_P );
        H5std_string TDATA_NAME( dataset_T );
        // open the file and datasets
        H5File file(FILE_NAME, H5F_ACC_RDONLY);
        DataSet dataset_p = file.openDataSet( PDATA_NAME );
        DataSet dataset_t = file.openDataSet( TDATA_NAME );
        // get the data spaces
        DataSpace datp_space = dataset_p.getSpace();
        DataSpace datt_space = dataset_t.getSpace();
        // read the rank and dimensions
        int rankp = datp_space.getSimpleExtentNdims();
        int rankt = datt_space.getSimpleExtentNdims();
        hsize_t dims_p[2], dims_t[2];
        int ndims = datp_space.getSimpleExtentDims( dims_p, NULL );
            ndims = datt_space.getSimpleExtentDims( dims_t, NULL );
        pbuff = new double[dims_p[0]*dims_p[1]];
        tbuff = new unsigned int[dims_t[0]*dims_t[1]];
        // read the array into the memory buffer
        dataset_p.read( pbuff, PredType::NATIVE_DOUBLE, DataSpace::ALL, 
                        datp_space );
        dataset_t.read( tbuff, PredType::NATIVE_UINT, DataSpace::ALL, 
                        datt_space );

        // copy the array from buffer to matrix
        m_p = new arma::mat(pbuff, (arma::uword) dims_p[0], (arma::uword) dims_p[1], true); 
        m_t = new arma::Mat<unsigned int>(tbuff,(arma::uword) dims_t[0], (arma::uword) dims_t[1], true); 
        m_Np = dims_p[0];
        m_Nt = dims_t[0];
        // clean up memory buffer
        delete[] pbuff;
        delete[] tbuff;
   }
   // catch failure caused by the H5File operations
   catch( FileIException error )
   {
      error.printError();
      return -1;
   }
   // catch failure caused by the DataSet operations
   catch( DataSetIException error )
   {
      error.printError();
      return -1;
   }
   // catch failure caused by the DataSpace operations
   catch( DataSpaceIException error )
   {
      error.printError();
      return -1;
   }
   // catch failure caused by the DataSpace operations
   catch( DataTypeIException error )
   {
      error.printError();
      return -1;
   }
   return 0;  // successfully terminated
}


int FEM_space::Write_HDF5(const std::string& filename,
                         const std::vector<double>& time,
                         const std::vector<double>& dataset_P,
                         const arma::mat& Data)
{
   const H5std_string FILE_NAME( filename );
   const H5std_string DATASET_TIME( "time" );
   const H5std_string DATASET_NONSP( "NonSP" );
   const H5std_string DATASET_W( "W_data" );

   double time_arr[time.size()], NonSP_arr[dataset_P.size()];
   double W_mat[Data.n_rows][Data.n_cols];

   // copy the vector to array for HDF5 write
   for ( unsigned int ind=0; ind<time.size(); ind++ ){
       time_arr[ind] = time[ind];
   }
   for ( unsigned int ind=0; ind<dataset_P.size(); ind++ ){
       NonSP_arr[ind] = dataset_P[ind];
   }
   for ( unsigned int rr=0; rr<Data.n_rows; rr++ ){
       for ( unsigned int cc=0; cc<Data.n_cols; cc++ ){
           W_mat[rr][cc] = Data(rr,cc);
       }
   }

   // create the file for data 
   H5File file(FILE_NAME, H5F_ACC_TRUNC);
   hsize_t dim_t[1], dim_NonSP[1], dim_data[2];
   dim_t[0] = time.size();
   dim_NonSP[0] = dataset_P.size();
   dim_data[0] = Data.n_rows;
   dim_data[1] = Data.n_cols;

   DataSpace dataspace_t(1,dim_t), dataspace_NonSP(1,dim_NonSP),
             dataspace_W(2,dim_data);

   DataType datatype(PredType::NATIVE_DOUBLE);
   DataSet dataset_t = file.createDataSet(DATASET_TIME,datatype,dataspace_t);
   DataSet dataset_NonSP = file.createDataSet(DATASET_NONSP,
       datatype,dataspace_NonSP);
   DataSet dataset_W = file.createDataSet(DATASET_W,datatype,dataspace_W);

   // read the array into the memory buffer
   dataset_t.write( time_arr, datatype );
   dataset_NonSP.write( NonSP_arr, datatype );
   dataset_W.write( W_mat, datatype );

   std::cout << "successful write to file" << std::endl;
   dataset_t.close();
   dataset_NonSP.close();
   dataset_W.close();
   dataspace_t.close();
   dataspace_NonSP.close();
   dataspace_W.close();
   file.close();
   return 0;  // successfully terminated
}
