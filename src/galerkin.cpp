#include <math.h>

#include<armadillo>

#include "FEM.hpp"
#include "galerkin.hpp"
#include "component.hpp"

arma::rowvec func_phi(unsigned int point, arma::Row<unsigned int>& t_k, 
                      double u, double v)
{
        unsigned int ind = 3;
        arma::rowvec::fixed<3> Phi;
        for(int pp=0; pp<3; pp++){
                if (t_k(pp) == point) ind = pp;
        }
        if ( ind == 0 ){
                 Phi(0) = -1.0;
                 Phi(1) = -1.0;
                 Phi(2) = 1.0 - u - v;
        }
        else if ( ind == 1 ){
                 Phi(0) = 0.0;
                 Phi(1) = 1.0;
                 Phi(2) = v;
        }
        else if ( ind == 2 ){
                 Phi(0) = 1.0;
                 Phi(1) = 0.0;
                 Phi(2) = u;
        }
        return Phi;
}

std::vector<double> Int_AB(unsigned int i, unsigned int j,
                           arma::Row<unsigned int>& tk, arma::rowvec& L,
                           arma::mat& gij, double diff)
{
         std::vector<double> sum_AB = { 0.0, 0.0 };
         double  G_ij = sqrt(arma::det(gij));
         arma::mat igij = inv(gij);
         
         // locations and weights of the Gaussian 2nd order integration point
         std::vector<double> k1 = { 1.0/3.0, 0.0597158717, 0.4701420641, 0.4701420641, 0.7974269853, 0.1012865073, 0.1012865073 };
         std::vector<double> k2 = { 1.0/3.0, 0.4701420641, 0.0597158717, 0.4701420641, 0.1012865073, 0.7974269853, 0.1012865073 };
         std::vector<double> k3 = { 1.0/3.0, 0.4701420641, 0.4701420641, 0.0597158717, 0.1012865073, 0.1012865073, 0.7974269853 };
         std::vector<double> weight = { 0.225, 0.1323941527, 0.1323941527, 0.1323941527, 0.1259391805, 0.1259391805, 0.1259391805 };

         // Evaluate the functions from seven integration points
         arma::rowvec::fixed<3> Phi_i, Phi_j;

         for (int pp = 0; pp < 7; pp++ ) {
                Phi_i = func_phi( i, tk, k3[pp], k2[pp] );
                Phi_j = func_phi( j, tk, k3[pp], k2[pp] );
                sum_AB[1] += weight[pp] * Phi_i(2) * Phi_j(2) * G_ij;
                sum_AB[0] += weight[pp] * G_ij * ( arma::dot(Phi_i.subvec(0,1),L)*Phi_j(2)
                                -diff * arma::dot(Phi_i.subvec(0,1) * igij, Phi_j.subvec(0,1)));
                
         }
         return sum_AB;
}


arma::rowvec local_coordinate(arma::rowvec& Xu, 
                              arma::rowvec& Xv,
                              arma::rowvec& Puv)
{
    arma::rowvec::fixed<3> fu, fv;
    arma::rowvec::fixed<2> torq_loc;
    double uu = arma::dot( Xu, Xu );
    double vv = arma::dot( Xv, Xv );
    double uv = arma::dot( Xu, Xv );

    fu = vv / ( uu*vv-uv*uv ) * Xu - uv / ( uu*vv - uv*uv ) * Xv;
    fv = -1.0*uv / ( uu*vv-uv*uv ) * Xu + uu / ( uu*vv - uv*uv ) * Xv;

    torq_loc(0) = arma::dot( Puv, fu );
    torq_loc(1) = arma::dot( Puv, fv );

    return torq_loc;
} 


void  FEM_space::galerkin( System& sys ){
          // define temperary variables
          arma::Row<unsigned int>::fixed<3> t_k;
          arma::rowvec::fixed<3> P1, Xu, Xv;
          arma::rowvec::fixed<2> L;
          arma::mat g_ij(2,2);
          double gA, gB, gC;
          unsigned int i, j;
          std::vector<double> sum_AB;

          matA = new arma::sp_mat(m_Np,m_Np); // mass matrice
          matB = new arma::sp_mat(m_Np,m_Np); // convection&stiffness matrices
          // assign non-zero elements
          for ( unsigned int k=0; k < m_Nt; k++ ){
                  t_k = m_t->row(k) - 1;
                  P1 = ( m_p->row( t_k(0) ) + m_p->row( t_k(1) )
                         + m_p->row( t_k(2) ) ) / 3.0;
                  Xu = m_p->row( t_k(2) ) - m_p->row( t_k(0) );
                  Xv = m_p->row( t_k(1) ) - m_p->row( t_k(0) );
                  arma::rowvec torque = sys.mag.getLocalTorq( P1, sys.injectCurr );
                  L = local_coordinate( Xu, Xv, torque);
                  gA = arma::dot( Xu, Xu );
                  gB = arma::dot( Xu, Xv );
                  gC = arma::dot( Xv, Xv );
                  g_ij(0,0) = gA; g_ij(0,1) = gB;
                  g_ij(1,0) = gB; g_ij(1,1) = gC;
                  for ( unsigned int ith = 0; ith < 3; ith++ ){
                          for ( unsigned int jth = 0; jth < 3; jth++){
                                  i = t_k(ith); j = t_k(jth);
                                  sum_AB = Int_AB( i, j, t_k, L, g_ij, sys.mag.getDiffusion(sys.getTemperature()) );
                                  (*matA)( i, j ) = (*matA)( i,j ) + sum_AB[0];
                                  (*matB)( i, j ) = (*matB)( i,j ) + sum_AB[1];
                          }
                  }
          }
}
