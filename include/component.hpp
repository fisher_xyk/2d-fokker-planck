#ifndef PARAM_H
#define PARAM_H

#include <string>

#include <armadillo>
#include "constants.hpp"


class Field {
        private:
                double          amplitude;
                arma::rowvec::fixed<3> Orient; 
        public:
                Field(): amplitude(0.0), Orient({0.0, 0.0, 1.0}) {};
                ~Field() {};
                Field(arma::rowvec orient_new);
                Field(double amp, arma::rowvec orient_new);

                inline double getAmplitude() const { return amplitude; };
                inline arma::rowvec getOrient() const { return Orient; };
                inline void setAmplitude(double amp_new);
                inline void setOrient(arma::rowvec orient_new);
};

class Current {
        private:
                double          Pol;
                Field           Spin;
                Field           Curr;
        public:
                Current(): Pol(1.0), Curr(), Spin() {};
                ~Current() {};
                Current(double pol, double curramp, 
                        arma::rowvec CurrOrient,
                        arma::rowvec SpinOrient);

                inline double getPol() const { return Pol; };
                inline double getCurrAmp() const { return Curr.getAmplitude(); };
                inline arma::rowvec getCurrOrient() const { return Curr.getOrient(); };
                inline arma::rowvec getSpinOrient() const { return Spin.getOrient(); };

                inline void setPol(double pol_new) { Pol = pol_new; return; };
                inline void setCurrAmp(double amp_new) { Curr.setAmplitude(amp_new); return; };
                inline void setCurrOrient(arma::rowvec orient_new);
                inline void setSpinOrient(arma::rowvec orient_new); 
};




class Magnet
{
        private:
                arma::rowvec::fixed<3> dims;                            // dimensions of the magnet
                double          damping;                                           // magnetic damping 
                double          OutOfPlane;
        
                Field           magnetization;
                Field           anisotropy;
                Field           demag;

                inline double getVolume() const { return 0.25 * C_PI * dims[0] * dims[1] * dims[2]; };
                inline double getArea() const { return 0.25 * C_PI * dims[0] * dims[1]; };
        public:
                Magnet();
                ~Magnet() {};
                inline arma::rowvec Minit() const { return magnetization.getOrient(); };
                void read_file(const std::string& filename);
                void print_info(double) const;
                arma::rowvec delta_xyz(double temperature) const;
                arma::rowvec getLocalTorq(const arma::rowvec&, const Current& ) const;
                inline double getDiffusion(double temperature) const;
                inline double getCritCurr() const;
                inline arma::rowvec getEffectiveDemag() const;
};

struct System
{
        private:
                double          temperature;                                            // temperature
                double          time, dt;
                unsigned int    nData;

        public:
                Magnet          mag;
                Current         injectCurr;
                Field           hext;
        
                System();
                ~System() {};
                void read_file(const std::string& filename);
                inline double getTemperature() const { return temperature; };
                inline double get_time_step(){ return dt; };
                void print_info() const;
                std::vector<double> sim_time();
                std::vector<unsigned int> record_inds(const std::vector<double> &time_grid);
};


inline void Field::setAmplitude(double amp_new) { amplitude = amp_new; return; }
                
inline void Field::setOrient(arma::rowvec orient_new) 
{ 
        Orient = norm(orient_new) == 0.0 ? orient_new : normalise(orient_new); 
}
                
inline void Current::setCurrOrient(arma::rowvec orient_new) 
{       
        Curr.setOrient(orient_new); 
        return; 
}

inline void Current::setSpinOrient(arma::rowvec orient_new) 
{ 
        Spin.setOrient(orient_new); 
        return; 
}

inline double Magnet::getDiffusion(double temperature) const
{ 
        double diff;
        diff = damping * C_kB * temperature * C_gyro 
                / (( 1.0+damping*damping ) * C_u0 
                   * magnetization.getAmplitude() * getVolume() );
        return diff;
}

inline arma::rowvec Magnet::getEffectiveDemag() const
{
        double Ms = magnetization.getAmplitude();
        double Vol = getVolume(), Ku = anisotropy.getAmplitude();
        return demag.getOrient() - 2.0 * Ku * Vol 
               / (C_u0*Ms*Ms*Vol) * anisotropy.getOrient();
}

inline double Magnet::getCritCurr() const
{
         double Icrit = 2.0 * damping * C_qe / ( C_hbar )
                        * anisotropy.getAmplitude() * getVolume() ;
         return Icrit;
}

#endif
