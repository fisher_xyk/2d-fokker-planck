#ifndef POST_H
#define POST_H

#include "component.hpp"
#include "FEM.hpp"
#include <armadillo>

std::vector<double> NSwitch_prob( arma::mat& Data, FEM_space& FEM, Magnet& mag);

#endif
