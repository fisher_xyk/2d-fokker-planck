#ifndef CONSTANTS_H
#define CONSTANTS_H

extern const double C_PI;
extern const double C_hbar;
extern const double C_u0;
extern const double C_qe;
extern const double C_gyro;
extern const double C_muB;
extern const double C_kB;


#endif
