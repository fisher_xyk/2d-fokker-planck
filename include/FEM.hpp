#ifndef FEM_H
#define FEM_H

#include <armadillo>
#include "component.hpp"
#include <string>

class FEM_space
{
      private:
          unsigned int m_Np;
          unsigned int m_Nt;
          arma::mat * m_p;
          arma::Mat<unsigned int> * m_t;
      public:
          arma::sp_mat* matA;
          arma::sp_mat* matB;

          FEM_space() {};
          ~FEM_space() {};
          int Read_HDF5(const std::string& filename,
                         const std::string& datap,
                         const std::string& datat);
          int Write_HDF5(const std::string& filename,
                         const std::vector<double>& time,
                         const std::vector<double>& NonSP,
                         const arma::mat& Data);
          inline unsigned int Grid_Npoints() const {return m_Np;};
          inline unsigned int Grid_Nmeshes() const {return m_Nt;};
          arma::mat* vertices();
          arma::Mat<unsigned int>* triang();
          void galerkin( System& sys );
};


#endif
