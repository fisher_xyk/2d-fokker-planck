#ifndef IO_H_
#define IO_H_

#include <cctype>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <armadillo>


int read_int(std::string line);

double read_double(std::string line); 

std::string read_string(std::string line);

arma::rowvec read_vector(std::string line, int length); 

int read_category(std::string key);

bool is_comment(std::string key);

std::string strip_category(const std::string key);

#endif
