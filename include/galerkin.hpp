#ifndef _GALERKIN_H_
#define _GALERKIN_H_

#include "component.hpp"
#include "FEM.hpp"

arma::vec  InitNodeValue( FEM_space& FEM, System& sys );


std::vector<double> Int_AB(unsigned int i, unsigned int j,
           arma::Row<unsigned int>& tk, arma::rowvec& L,
           arma::mat& gij, double diff);


double integrate_node( const arma::mat& p, const arma::Row<unsigned int>& t_k, const arma::vec& W );


#endif
